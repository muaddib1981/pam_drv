#include <stdio.h>
#include <string.h>
#include <erl_driver.h>
#include <sstream>
#include <stdarg.h>
#include <vector>
#include "pamlogin.h"

typedef struct {
    ErlDrvPort port;
} erl_data;

const char parseError[] = "-100";

std::string pam_parse(std::string inbuf)
{
	std::stringstream ss(inbuf);
    std::string str;
    char cmd[20];
    unsigned int handle = 0;
    std::string proto, login, password;
    ss >> proto;
    if (ss.fail())
       return parseError;

    if (proto == "tac_plus")
    {
    	ss >> handle;
    	if (ss.fail())
    	   return parseError;

    	ss >> login;
    	if (ss.fail())
    	   return parseError;

		ss >> password;
    	if (ss.fail())
    	   return parseError;

    	return (authenticate_tacplus(handle, login, password));
    }
    else
    if (proto == "tac_cmd")
    {
      	ss >> handle;
        ss.seekg(1,ss.cur);
        ss.getline(cmd, 20);

      	return (exec_tac_cmd(handle, cmd));
    }
    else
    if (proto == "rad_cmd")
    {
      	ss >> handle;
        ss.seekg(1,ss.cur);
        ss.getline(cmd, 20);

      	return (exec_rad_cmd(handle, cmd));
    }
    else
    if (proto == "radius")
    {
        ss >> handle;
    	if (ss.fail())
    	   return parseError;

    	ss >> login;
    	if (ss.fail())
    	   return parseError;

    	ss >> password;
    	if (ss.fail())
    	   return parseError;

    	return (authenticate_radius(handle, login, password));
    }
    else
    if (proto == "local")
    {
        ss >> handle;
    	if (ss.fail())
    	   return parseError;

    	ss >> login;
    	if (ss.fail())
    	   return parseError;

    	ss >> password;
    	if (ss.fail())
    	   return parseError;

    	return (authenticate_local(handle, login, password));
    }
    else
    if (proto == "logout")
    {
    	ss >> handle;
    	if (ss.fail())
    		return std::string("failed");
    	int res = logout(handle);
    	if (res == 0)
    	   return (std::string("ok"));
  	    return (std::string("failed"));
    }
    else
    if (proto == "logoutall")
    {
        logoutall();
        return (std::string("ok"));
    }
    else
    if (proto == "list")
    {
    	return list();
    }


    return (parseError);
};

extern "C" ErlDrvData erldrv_start(ErlDrvPort port, char *buff)
{
    erl_data* d = (erl_data*)driver_alloc(sizeof(erl_data));
    d->port = port;
    return (ErlDrvData)d;
}

extern "C" void erldrv_stop(ErlDrvData handle)
{
    driver_free((char*)handle);
}

extern "C" void erldrv_output(ErlDrvData handle, char *buff,
			       ErlDrvSizeT bufflen)
{
	erl_data* d = (erl_data*)handle;
	std::string str = pam_parse(buff);
    driver_output(d->port, (char*)str.c_str(), str.size());
}

ErlDrvEntry erldriver_entry = {
    NULL,			// F_PTR init, called when driver is loaded
    erldrv_start,		// L_PTR start, called when port is opened
    erldrv_stop,		// F_PTR stop, called when port is closed
    erldrv_output,		// F_PTR output, called when erlang has sent
    NULL,			// F_PTR ready_input, called when input descriptor ready
    NULL,			// F_PTR ready_output, called when output descriptor ready
    "pam_drv",		// char *driver_name, the argument to open_port
    NULL,			// F_PTR finish, called when unloaded
    NULL,                       // void *handle, Reserved by VM
    NULL,			// F_PTR control, port_command callback
    NULL,			// F_PTR timeout, reserved
    NULL,			// F_PTR outputv, reserved
    NULL,           // F_PTR ready_async, only for async drivers
    NULL,           // F_PTR flush, called when port is about to be closed, but there is data in driver queue
    NULL,           // F_PTR call, much like control, sync call to driver
    NULL,           // F_PTR event, called when an event selected  by driver_event() occurs.
    ERL_DRV_EXTENDED_MARKER,    // int extended marker, Should always be set to indicate driver versioning
    ERL_DRV_EXTENDED_MAJOR_VERSION, // int major_version, should always be set to this value */
    ERL_DRV_EXTENDED_MINOR_VERSION, /* int minor_version, should always be
				       set to this value */
    0,                          /* int driver_flags, see documentation */
    NULL,                       /* void *handle2, reserved for VM use */
    NULL,                       /* F_PTR process_exit, called when a
				   monitored process dies */
    NULL                        /* F_PTR stop_select, called to close an
				   event object */
};

extern "C" DRIVER_INIT(pam_drv) /* must match name in driver_entry */
{
    return &erldriver_entry;
}




