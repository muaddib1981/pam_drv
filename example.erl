-module(example).
-export([start/1, stop/0, init/1]).
-export([foo/1, bar/1]).
-export([auth_tacplus/3, auth_radius/3, auth_local/3, logout/1, logoutall/0, list/0, tac_cmd/2, rad_cmd/2]).
-define(TAC_PLUS,"tac_plus").
-define(RADIUS,"radius").
-define(LOCAL,"local").
-define(LOGOUT,"logout").
-define(LOGOUTALL,"logoutall").
-define(LIST,"list").
-define(TAC_CMD,"tac_cmd").
-define(RAD_CMD,"rad_cmd").

start(SharedLib) ->
    case erl_ddll:load_driver(".", SharedLib) of
    ok -> ok;
    {error, already_loaded} -> ok;
    {error, R} ->
	io:format("~p~n",[R]), 
%%	exit({error, could_not_load_driver})
        exit({error, erl_ddll:format_error(R)})
    end,
    spawn(?MODULE, init, [SharedLib]).

init(SharedLib) ->
    register(complex, self()),
    Port = open_port({spawn, SharedLib}, []),
    loop(Port).

stop() ->
    complex ! stop.

foo(X) ->
    call_port({foo, X}).
bar(Y) ->
    call_port({bar, Y}).
auth_tacplus(Handle,Login,Password) ->
    call_port({auth_tacplus, Handle, Login, Password}).
auth_radius(Handle, Login, Password) ->
    call_port({auth_radius, Handle, Login, Password}).
auth_local(Handle, Login, Password) ->
    call_port({auth_local, Handle, Login, Password}).
logout(H) ->
    call_port({logout, H}).
logoutall() ->
    call_port({logoutall}).
list() -> 
    call_port({list}).
tac_cmd(H, Cmd) ->
    call_port({tac_cmd, H, Cmd}).
rad_cmd(H, Cmd) ->
    call_port({rad_cmd, H, Cmd}).

call_port(Msg) ->
    complex ! {call, self(), Msg},
    receive
    {complex, Result} ->
        Result
    end.

loop(Port) ->
    receive
    {call, Caller, Msg} ->
        Port ! {self(), {command, encode(Msg)}},
        receive
	{Port, {data, Data}} ->
	    Caller ! {complex, decode(Data)};
	Result -> io:format("~p~n", [Result])
        end,
        loop(Port);
    stop ->
        Port ! {self(), close},
        receive
	{Port, closed} ->
	    exit(normal)
        end;
    {'EXIT', Port, Reason} ->
        io:format("~p ~n", [Reason]),
        exit(port_terminated)
    end.

encode({foo, X}) -> [1, X];
encode({bar, Y}) -> [2, Y];
%tac_plus, username, password, command
encode({auth_tacplus, Handle, Login, Password}) -> [?TAC_PLUS," ",Handle," ",Login," ",Password,0];
encode({auth_radius, Handle, Login, Password}) -> [?RADIUS," ",Handle," ",Login," ",Password,0];
encode({auth_local, Handle, Login, Password}) -> [?LOCAL," ",Handle," ",Login," ",Password,0];
encode({logoutall}) -> [?LOGOUTALL,0];
encode({list}) -> [?LIST,0];
encode({tac_cmd, H, Cmd}) -> [?TAC_CMD," ",H," ",Cmd,0];
encode({rad_cmd, H, Cmd}) -> [?RAD_CMD," ",H," ",Cmd,0];
encode({logout, H}) -> [?LOGOUT," ",H,0].

decode(String) -> String.